# ABC iview shows Recommender System

## Table of contents 
1. Purpose
2. Installation
3. Usage and Features
4. Suggestions for Future Updates
5. Bugs
6. Contact information
7. License

## Purpose 
The objective of this study is to build a Recommender System for ABC iview shows based on values of different stakeholders. In order to find these specific values, a stakeholder analysis is performed. This resulted in the following main stakeholders: ABC and the public. To sketch values for the public, 5 persona's are created. From these 5 persona's, 1000 users are generated in order to create and use the Recommender System. There are two possibilities to use this Recommender System: logged-in and not. 

## Installation
`` Note: This installation is for Python (version 3) ``

### Clone the project 
To execute the project, this repository needs to be cloned. 
```
git clone https://mlbloembergen@bitbucket.org/mlbloembergen/infoppm.git
```

### Packages 
To execute the files, several packages need to be imported. The importation is as follows:

```
import package_name
```

This is also included in the python files and should be installed before running the streamlit application. 

## Usage and Features
This repository contains different files. 

* The **app** directory containing two subdirectories **data** and **JSON_files** where the data directory contains the ABC iview data, cosine distances and generated users information with their show likelihood. The JSON_files directory contains generated persona's, users and passwords. Besides these directories, the **app** directory contains python files `app.py`, `authenticate.py` and `template.py`. These files run the Recommender System containing the specific values. You can run the Recommender System by running the following in the terminal:

```
pip install streamlit
streamlit run app.py
```

* The **code** directory containing three subdirectories **content_analysis**, **generating_users** and **user_research** where scripts are stored which are used for the content analysis, generating users and research of users. These scripts are interwoved in the **app** directory for the Recommender System or used for research applied in the report.

* This `README.md`

* As explained in the purpose and in more debt in the report, 1000 users are generated and rendered based on watch behaviour. 
In order to run the Recommender System, we selected 5 users for you (each in one persona). 

`Persona A - id: id8 - password: battement751747`

`Persona B - id: id3 - password: well-judged69536`

`Persona C - id: id16 - password: tagster815696`

`Persona D - id: id12 - password: preliterates199020`

`Persona E - id: id14 - password: goyish16522`

## Suggestions for Future Updates
* Generating more users to increase the probabilities 

## Bugs
* None

## Contact information 
👤 Nelly A.S. Dua, Sander Engelberts, Bruno Laiber De Pinho, Jo A. Schreurs, Mar Liesje Bloembergen 

## License
This project is [MIT](https://choosealicense.com/licenses/mit/) licensed. 
