import streamlit as st
import pandas as pd
import template as t
import authenticate as a
import json
from itertools import cycle
from random import random

# page layout and thumbnail browser
st.set_page_config(layout='wide', page_title='ABC iview recommendations', page_icon='https://static.wikia.nocookie.net/logopedia/images/f/f8/IView_2011-icon.svg')

# logo and main title
logo, page_header = st.columns([2,10]) 
with logo:
  st.write("")   
  st.write("")
  st.image('https://static.wikia.nocookie.net/logopedia/images/7/74/Iview-2018.svg', width=125)
with page_header:
  st.title('Prototype Recommender System for ABC - assignment 2')

# ABC content
df            = pd.read_csv('./data/clustered_representation_shows.csv', sep=',')
df_dist       = pd.read_csv('./data/cosine_distances.csv', sep=',')
# df_users      = pd.read_json('users.json')
# df_activities = pd.read_json('activities.json')
df_episodes   = pd.read_csv('data/clean_episode_data_v2.csv')
df_show_like  = pd.read_csv('data/show_likelihood.csv')
df_new_user_info = pd.read_csv('data/new_user_info.csv')


# open the activities json file
with open('activities.json') as json_file:
  users_activities = json.load(json_file)

if 'id' not in st.session_state:
  st.session_state['id'] = 51

if 'episode' not in st.session_state:
  st.session_state['episode'] = 10001
    
if 'user' not in st.session_state:
  st.session_state['user'] = 1001

if 'activities' not in st.session_state:
  st.session_state['activities'] = users_activities

# authenticate
a.authenticate()

# Display Show or Episode informations and image
if st.session_state['activities'][-1]['content_id'] > 10000:
    df_id = df_episodes[df_episodes['epi_id'] == st.session_state['episode']].iloc[0]
    print('User   :', st.session_state['user'])
    print('ID     :', st.session_state['id'])
    print('Episode:', st.session_state['episode'])
    print('last id (show or episode):', st.session_state['activities'][-1]['content_id'])
    print('Title  :', df_id['episode_title'])
    print('- - - - -')

    col1, col2 = st.columns(2)

    with col1:
      st.image(df_id['image_url'], use_column_width='always')

    with col2:
      st.title(df_id['episode_title'])
      st.caption(df[df['id'] == st.session_state['id']]['genre'].iloc[0])
      st.markdown(df_id['description'])
      st.button('👍', key=random(), on_click=t.activity, args=(int(df_id['epi_id']), 'Like'))  
      st.button('👎', key=random(), on_click=t.activity, args=(int(df_id['epi_id']), 'Dislike'))
else:
    df_id = df[df['id'] == st.session_state['id']].iloc[0]
    print('User   :', st.session_state['user'])
    print('ID     :', st.session_state['id'])
    print('Title  :', df_id['title'])
    print('last id (show or episode):', st.session_state['activities'][-1]['content_id'])
    print('- - - - -')

    col1, col2 = st.columns(2)

    with col1:
      st.image(df_id['image_url'], use_column_width='always')

    with col2:
      st.title(df_id['title'])
      st.caption(df_id['genre'])
      st.markdown(df_id['description'])
      st.button('👍', key=random(), on_click=t.activity, args=(int(df_id['id']), 'Like'))  
      st.button('👎', key=random(), on_click=t.activity, args=(int(df_id['id']), 'Dislike'))

   
# SIDEBAR (SURPRISE ME)
st.sidebar.header('Recommendation settings')
content_diversity = st.sidebar.selectbox('Surprise me?',
     ('Yes', 'No'), index = 1, help = "Do you want to be surprised with content that is not similar to the shows you have already watched or liked before? Select 'yes'.") 



# Recommendations - row 1 (EPISODES)
len_episodes = len(df_episodes[df_episodes['show_id'] == st.session_state['id']])

if len_episodes > 0:    
    with st.expander('More episodes of ' + df[df['id'] == st.session_state['id']]['title'].iloc[0]):
      if len_episodes > 6:
        t.tiles_epi(df_episodes[df_episodes['show_id'] == st.session_state['id']].sample(6))
      else:
        t.tiles_epi(df_episodes[df_episodes['show_id'] == st.session_state['id']])
       
        
# Recommendations - row 2 (SURPRISE ME)
if content_diversity == 'Yes':    
    with st.expander('Surprise Me!'):
      indig = df[df['indigenous_representation'] == 1].sample(1)
      bipoc = df[df['bipoc_representation'] == 1].sample(1)
      lgbtq = df[df['lgbtq_representation'] == 1].sample(1)
      id2surprise = df_dist[(df_dist['id_a'] == st.session_state['id']) & (df_dist['cosine_distance'] == 0)].sample(3)['id_b'].to_list()
      filtered_cosine0  = df[df['id'].isin(id2surprise)]
      t.tiles(indig.append([bipoc, lgbtq, filtered_cosine0]))

        
# Recommendations - row 3 (User similarities)
if st.session_state['user'] < 1000:
    with st.expander('What your peers are watching'):
        ids2filter = df_show_like[df_show_like['user_id'] == st.session_state['user']].sort_values('like_probability', ascending = False).head(10)['show_id'].sample(6).to_list()
        print('User: ' + str(st.session_state['user']))
        print('ids to filter (user)   : ' + str(ids2filter))
        filtered_user  = df[df['id'].isin(ids2filter)]
        t.tiles(filtered_user)

        
# Recommendations - row 4 (COSINE + CLUSTER)
with st.expander('Shows similar to'):
    # based on cosine
    ids2filter = df_dist[df_dist['id_a'] == st.session_state['id']].sort_values('cosine_distance', ascending = False).head(3)['id_b'].to_list()
    print('ids to filter (cosine)   : ' + str(ids2filter))
    filtered_cosine  = df[df['id'].isin(ids2filter)]
    
    # based on cluster
    cluster2filter = df[df['id'] == st.session_state['id']]['k_means_cluster'].iloc[0]
    print('Filtered based on cluster: ' + str(cluster2filter))    
    filtered_cluster = df[(df['k_means_cluster'] == cluster2filter) & (df['id'] != st.session_state['id'])].sample(3)
    t.tiles(filtered_cosine.append(filtered_cluster))


# Recommendations - row 5 (BECAUSE YOU LIKED)
if st.session_state['user'] < 1000:
    user_filter_id1 = int(df_new_user_info[(df_new_user_info['id'] == st.session_state['user']) & (df_new_user_info['liked_shows']).notnull()]['liked_shows'].sample(1).iloc[0])

    with st.expander('Because you liked ' + df[df['id'] == user_filter_id1]['title'].iloc[0] + ' 👍'):
      ids2filter = df_dist[df_dist['id_a'] == user_filter_id1].sort_values('cosine_distance', ascending = False).head(6)['id_b'].to_list()
      print('ids to filter (liked)    : ' + str(ids2filter))
      t.tiles(df[df['id'].isin(ids2filter)])


# Recommendations - row 6 (BECAUSE YOU WATCHED)
if st.session_state['user'] < 1000:
    user_filter_id2 = int(df_new_user_info[(df_new_user_info['id'] == st.session_state['user']) & (df_new_user_info['also_clicked_shows']).notnull()]['also_clicked_shows'].sample(1).iloc[0])

    with st.expander('Because you watched ' + df[df['id'] == user_filter_id2]['title'].iloc[0]):
      ids2filter = df_dist[df_dist['id_a'] == user_filter_id2].sort_values('cosine_distance', ascending = False).head(6)['id_b'].to_list()
      print('ids to filter (watched)  : ' + str(ids2filter))
      t.tiles(df[df['id'].isin(ids2filter)])


# Recommendations - row 7 (REPRESENTATION DIVERSE)
if st.session_state['user'] < 1000:
    with st.expander('Inclusive and diverse shows'):
        ids_representation = df[df['representation_diverse'] == 1]['id'].to_list()
        ids2representation = df_show_like[(df_show_like['user_id'] == st.session_state['user']) & (df_show_like['show_id'].isin(ids_representation))].sort_values('like_probability', ascending = False).head(6)['show_id'].to_list()
        t.tiles(df[df['id'].isin(ids2representation)])
else:
    with st.expander('Inclusive and diverse shows'):
        ids_representation = df[df['representation_diverse'] == 1]['id'].to_list()
        ids2representation = df_dist[(df_dist['id_b'].isin(ids_representation)) & (df_dist['id_a'] == st.session_state['id'])].sort_values('cosine_distance', ascending = False).head(6)['id_b'].to_list()
        t.tiles(df[df['id'].isin(ids2representation)])

print('. . . . . . . . . . END . . . . . . . . . . ')
print('')


